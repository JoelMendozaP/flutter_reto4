import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ParaTi extends StatefulWidget {
  @override
  _ParaTiState createState() => _ParaTiState();
}

class _ParaTiState extends State<ParaTi> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarBrightness: Brightness.dark,
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      systemNavigationBarIconBrightness: Brightness.dark
    )
  );
    return ListView(
      children: <Widget>[
        _titulo(),
        _apps(),
        _tituloSinFlecha(),
        _apps(),
        _titulo(),
        _apps(),
      ],
    );
  }
  Widget _titulo(){
    return Container(
      padding: EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('Juegos de caricaturas',style: TextStyle(fontSize: 17,color: Colors.black87,fontWeight: FontWeight.w600),),
          Icon(Icons.arrow_forward,color: Colors.black87,),
        ],
      ),
    );
  }
  Widget _tituloSinFlecha(){
    return Container(
      padding: EdgeInsets.all(20),
      child: Text.rich(
      TextSpan(
        children: <TextSpan>[
          TextSpan(text: 'Anuncios . ',style: TextStyle(fontSize: 12,color: Colors.black87,fontWeight: FontWeight.w700)),
          TextSpan(text: 'Sugerencias para ti',style: TextStyle(fontSize: 17,color: Colors.black87,fontWeight: FontWeight.w600)),
        ],
      ),
    )
    );
  }
  Widget _apps(){
    return Container(
      //color: Colors.red,
      height: 150,
      //width: 250,
      margin: EdgeInsets.only(left: 15),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          _app(),
          _app(),
          _app(),
          _app(),
          _app(),
        ],
      ),
    );
  }
  Widget _app(){
    return Container(
      padding: EdgeInsets.only(left: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _cardimg(),
          _tituloapp(),
          _pesoapp()
        ],
      ),
    );
  }
  Widget _cardimg(){
    return Card(
      elevation: 8,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50)
        ),
      child: _imagen()
    );
  }
  Widget _imagen(){
    return Container(
      color: Colors.yellowAccent,
      child: Image(
            width: 90,
            height: 90,
            fit: BoxFit.cover,
            image:AssetImage('assets/img/4.jpg') 
        ),
    );
  }
  Widget _tituloapp(){
    return Container(
      padding: EdgeInsets.all(5),
      child: Text(
        'Preguntados',
        style: TextStyle(),
      ),
    );
  }
  Widget _pesoapp(){
    return Container(
      padding: EdgeInsets.only(left: 5),
      child: Text(
        '44 MB',
        style: TextStyle(
          color: Colors.black54,
          fontSize: 12
        ),
      ),
    );
  }
}