import 'package:flutter/material.dart';
import 'package:flutter_reto4/Juegos/parati.dart';
class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  TabController _controller;
  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 5, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.blue,
      body: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black45,
                  offset: Offset(0,1),
                  blurRadius: 3
                )
              ]
            ),
            child: TabBar(
              unselectedLabelColor: Colors.black54,
              labelColor: Color.fromARGB(255, 23, 132, 30),
              indicatorSize: TabBarIndicatorSize.label,
              indicatorColor: Color.fromARGB(255, 23, 132, 30),
              isScrollable: true,
              labelStyle: TextStyle(
                  fontWeight: FontWeight.w600,
              ),
              controller: _controller,
              tabs: [
                Tab(text: 'Para ti'),
                Tab(text: 'Listas de exito'),
                Tab(text: 'Premiun'),
                Tab(text: 'Categorias'),
                Tab(text: 'Familia'),
              ],
            ),
          ),
          new Container(
            height: 596,
            color: Colors.white,
            child: new TabBarView(
              controller: _controller,
              children: <Widget>[
                Container(
                  child: ParaTi(),
                ),
                Container(
                  child: Text('data'),
                ),
                Container(
                  child: Text('data'),
                ),
                Container(
                  child: Text('data'),
                ),
                Container(
                  child: Text('data'),
                ),
              ],
            ),
          ),
        ],
      ),  
    );
  }
}